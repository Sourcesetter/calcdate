# calculates the number of working days between to given dates

# How to install, example
% cp calcdate calcDateLib.pl feiertage.pl $HOME/bin

# Start with
% calcdate -h

# Try with
% calcdate -s 1.11.2019 -e 04.11.2019 -kv


# Credits to  2001 by Peter Baumann (2001) for writing feiertage.pl
