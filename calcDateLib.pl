use DateTime;
use Env;
require "$HOME/bin/feiertage.pl";
$undefyear=1899;

sub listDays_Ref{
	# use: listDays(startDate, endDate)
	# startDate and endDate are objects of class DateTime:
	# Lists Days between two dates startDate und endDate
	# Uebergabe in per Array @_
	# Thus the first argument to the function is in $_[0],
	# the second is in $_[1], and so on.
	#
	# Hand over of the addresses
	my $startDate = @_[0];
	my $endDate = @_[1];

	my $counter=0;
	while ($startDate <= $endDate){
		$counter++;
		print "listDaysRef-Startdatum: ",$startDate," Endedatum: ",$endDate," Zaehler: ",$counter,"\n";
		$startDate = $startDate->add(days => 1);
   }
   return $counter;
}

sub listDays_Val{
	# use: listDays_Val(startDate, endDate)
	# Lists Days between two dates startDate und endDate
	# Uebergabe in per Array @_
	# Thus the first argument to the function is in $_[0],
	# the second is in $_[1], and so on.
	#
	# Hand over of the values
	my $startDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);
	my $endDate = DateTime->new(
				year   => @_[1]->year,
				month  => @_[1]->month,
				day    => @_[1]->day,
		);

	my $counter=0;
	while ($startDate <= $endDate){
		$counter++;
		print "listDays_Val: Startdatum: ",$startDate->ymd," Endedatum: ",$endDate->ymd," Zaehler: ",$counter, " Wochentag: ",$startDate->day_of_week, " Tag im Jahr: ", $startDate->day_of_year, "\n";
		$startDate = $startDate->add(days => 1);
   }
   return $counter;
}

sub daysBetween{
	# use: daysBetween(startDate, endDate)
	# Returns the number of days between startDate und endDate
	#
	# Hand over of the values
	my $startDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);
	my $endDate = DateTime->new(
				year   => @_[1]->year,
				month  => @_[1]->month,
				day    => @_[1]->day,
		);

	my $counter=0;
	while ($startDate <= $endDate){
		$counter++;
		$startDate = $startDate->add(days => 1);
   }
   return $counter;
}

sub weekdaysBetween{
	# use: weekdaysBetween(startDate, endDate)
	# Returns the number of weekdays between startDate und endDate
	# Days without Saturdays or Sundays
	#
	# Hand over of the values
	my $startDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);
	my $endDate = DateTime->new(
				year   => @_[1]->year,
				month  => @_[1]->month,
				day    => @_[1]->day,
		);

	my $counter=0;
	while ($startDate <= $endDate){
			if ($startDate->day_of_week <= 5){
				$counter++;
			}
		$startDate = $startDate->add(days => 1);
   }
   return $counter;
}

sub isPublicHoliday{
	# use: isPublicHoliday(inDate, region)

	my $inDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);

	my $region = @_[1];

	my $holmonth = 0;
	my $holday = 0;
	my @tmp = 0;
	my $i;
	my $workDate = DateTime->new(
				year   => @_[0]->year,
				month  => 01,
				day    => 02,
		);
	my @publicHoliday;
	my $key;


	# calculate array with public holiday
	my %f=&feiertage($inDate->year,$region);

	# Change format into date format
	foreach $key (keys %f){
			# print "Key: ",$key," value: ",$f{$key},"\n";
			@tmp = split(/\./,$f{$key});
 			$holday = @tmp[0];
        	$holmonth  =  @tmp[1];
        	$holyear  =  $inDate->year;
			$workDate->set_year($holyear);
			$workDate->set_month($holmonth);
			$workDate->set_day($holday);
			push(@publicHoliday,$workDate->clone());
			splice(@tmp, 0);
			$workDate=initDate();

	}

	foreach $i (@publicHoliday){
		# print $i->ymd('-'),"\n";
		if ( $i == $inDate){
			return(1);
		}
	}
	return(0);
}

sub workdaysBetween{
	# useorkdaysBetween(startDate, endDate, county)
	# Returns the number of weekdays between startDate und endDate
	#
	# Hand over of the values
	my $startDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);
	my $endDate = DateTime->new(
				year   => @_[1]->year,
				month  => @_[1]->month,
				day    => @_[1]->day,
		);
	my $region = @_[2];

	my $counter=0;
	while ($startDate <= $endDate){
			if ($startDate->day_of_week <= 5){
				if (isPublicHoliday($startDate,$region) == 0){
					$counter++;
				}
			}
		$startDate = $startDate->add(days => 1);
   }
   return $counter;
}

sub splitIntoMonths{
	# splitIntoMonth(startDate, endDate)
	# Returns 2019-06-28 until 2019-07-03
	# returns 2019-06-28,2019-06-30,2019-07-01,2019-07-03
	my $startDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);
	my $endDate = DateTime->new(
				year   => @_[1]->year,
				month  => @_[1]->month,
				day    => @_[1]->day,
		);
	my $tmpDate = DateTime->new(
				year   => @_[0]->year,
				month  => @_[0]->month,
				day    => @_[0]->day,
		);
	my $errorDate = DateTime->new(
				year   => 1999,
				month  => 9,
				day    => 9,
		);

	my @daylist;

	if ($startDate->year != $endDate->year){
			print "splitIntoMonths: subroutine can work on dates of one year only \n!";
			$errorDate->set_day(9);
			push(@daylist,$errorDate->clone());
			return @daylist;
	}
	if ($startDate->month > $endDate->month){
			print "splitIntoMonths: subroutine can work with inconsistent months \n!";
			$errorDate->set_day(10);
			push(@daylist,$errorDate->clone());
			return @daylist;
	}
	if ($startDate->month == $endDate->month){
			push(@daylist,$startDate->clone());
			push(@daylist,$endDate->clone());
			return @daylist;
	}
	if ($startDate->month < $endDate->month){
			push(@daylist,$startDate);
			$i=$startDate->month;
			while($i < $endDate->month){
				# Ausgabe des Monatsletzten
				$tmpDate=DateTime->last_day_of_month(year => $startDate->year, month => $i );
				push(@daylist,$tmpDate->clone());
				# print "splitIntoMonths i:",$i," tmpdate: ", $tmpDate->ymd, "\n";
				$i++;
				# hier muss noch der Monatserste zum Monatsletzten ausgegeben werden
				# unbedingt erst den Tag zuerst auf 1 setzen
				$tmpDate->set_day(1);
				$tmpDate->set_month($i);
				push(@daylist,$tmpDate->clone());

			}
			# Wenn das letzte tmpDate == endDate, dann Zeitraum von einem Tag OK.
			push(@daylist,$endDate->clone());
			return @daylist;
	}

}

sub string2date{
	# Converts standard European date strings into date objects
	# Use string2date(1.11.2019)
	# Returns date object with content of string 2019-11-01
	@inda = split(/\./, $_[0]);

	my $outdate = DateTime->new(
		year   => 1990,
		month  => 1,
		day    => 1,
	);

	my $inyear =$inda[2];
	my $inmonth=$inda[1];
	my $inday  =$inda[0];

	if($inyear < 1990  or $inmonth > 12 or $inmonth < 1 or $inday < 1 or $inday > 31){
			print "string2date: Date not plausible: ",$inday,".",$inmonth,".",$inyear,"\n";
			$outdate->set( year => $undefyear);
			return $outdate;
	}
	my $tmpDate = DateTime->new(
		year   => 1900,
		month  => 1,
		day    => 1,
	);

	$tmpDate = DateTime->last_day_of_month(year => $inyear, month => $inmonth );
	if ($inday > $tmpDate->day ){
			print "string2date: Day in month not plausible: ",$inday,".",$inmonth,".",$inyear,"\n";
			$outdate->set( year => $undefyear);
			return $outdate;
	}


	$outdate->set( year => $inyear );
	$outdate->set( month => $inmonth );
	$outdate->set( day => $inday );

	return $outdate;
}

sub initDate{
	# Low-Value Belegung eines Date Objekts
	my $outdate = DateTime->new(
		year   => 1990,
		month  => 1,
		day    => 1,
	);
	return $outdate;
}

sub checkDate{
	# checkDate(InDate)
	# 0 of OK, else < 0 depending on case
	# advanced checks of dates
	#
	my $inDate = DateTime->new(
		year   => @_[0]->year,
		month  => @_[0]->month,
		day    => @_[0]->day,
	);

	my $tmpDate = DateTime->new(
		year   => 1900,
		month  => 1,
		day    => 1,
	);

	$tmpDate = DateTime->last_day_of_month(year => $inDate->year, month => $inDate->month );
	if ($inDate->day > $tmpDate->day ){
			print "checkDate: day not plausible: ",$inDate->ymd,"\n";
			return -5;
	}

	return 0;
}

1;
