sub feiertage{

# Aufruf
#
# require 'feiertage.pl';
#	%f=&feiertage($jahr,$region);
#
#   $jahr=gewuenschtes Jahr
#
#	$region = Leer = Deutschland
#			= Bayern: Deutschland + Lokale Feiertage Bayerns
#			= falls erweitert wird: Regionen getrennt durch Leerzeichen ' '
#
#	Einsatzbereich des Scriptes 1.1.1970  - 31.12.2038, diese Limitierung erfolgt durch timelocal()
#	auf neueren Systemen kann dieses Enddatum aber durchaus ueberschritten werden
#
#	R�ckgabewerte
#	alle 	%feiertage{Name des Feiertages}="tag.monat.";
#

use strict;
use Time::Local;
# require 'timelocal.pl';

my $eintag=86400; #Sekunden des Tages
my $jahr=shift;
my $region=shift;
my ($i,$j,$c,$h,$g,$l,$EasterMonth,$EasterMonth,$EasterDay,$EasterDate,$day,$mday,$mon,$wert);
my %feiertage=();
#
# copyright 2001 by Peter Baumann
# Verwendung und Weitergabe nur gestattet, wenn der Hinweis auf den Ersteller dieses Scriptes erhalten bleibt
#
 $g = $jahr % 19;
 $c = int($jahr/100);
 $h = ($c - int($c/4)-int((8*$c+13)/25)+ 19*$g + 15) % 30;
 $i = $h - int($h/28)*(1 -int($h/28)*int(29/($h+1))*int((21 - $g)/11));
 $j = ($jahr + int($jahr/4) + $i + 2 - $c + int($c/4));
 $j = $j % 7;


$l = $i - $j;
$EasterMonth = 3 + int(($l+40)/44);
$EasterDay = $l + 28 - 31*int($EasterMonth/4);

if ($EasterMonth==3) {
        $mday=$EasterDay;
        $mon=3;
   $EasterDate = 'March ' . $EasterDay . ', ' . $jahr;
   }
else {  $mday=$EasterDay;
        $mon=4;
   $EasterDate = 'April ' . $EasterDay . ', ' . $jahr;}



my $epoche=&maketime($mday,$mon,$jahr);
my $datum=&getdays($epoche); # Das w�re der Ostersonntag, denn danach richten
							 # sich alle religi�sen Feiertage wie Ostern,
							 # Pfingsten, Fronleichnam, Himmelfahrt oder auch
							 # "Rosenmontag", der 7 Wochen vor Ostermontag ist

### Feste Feiertage, das kann ja jeder ...
$feiertage{'Neujahr'}="01.01.";
$feiertage{'Tag der Arbeit'}="01.05.";
$feiertage{'Tag der deutschen Einheit'}="03.10.";
$feiertage{'1. Weihnachtstag'}="25.12.";
$feiertage{'2. Weihnachtstag'}="26.12.";

my $owert=$epoche;

###
$wert=$owert-2*$eintag;
$datum=&getdays($wert);
$feiertage{'Karfreitag'}=$datum;
###
$wert=$owert+$eintag;
$datum=&getdays($wert);
$feiertage{'Ostermontag'}=$datum;
###
$wert=$owert+49*$eintag;
$datum=&getdays($wert);
$feiertage{'Pfingstsonntag'}=$datum;
###
$wert=$owert+50*$eintag;
$datum=&getdays($wert);
$feiertage{'Pfingstmontag'}=$datum;
###
$wert=$owert+39*$eintag;
$datum=&getdays($wert);
$feiertage{'Christi Himmelfahrt'}=$datum;

###

my @region=split(/ /,$region);

foreach $region (@region){

	## Region Bayern ###
	if ($region eq 'Bayern'){
		$wert=$owert+60*$eintag;
		$datum=&getdays($wert);
		$feiertage{'Fronleichnam'}=$datum;
		###
		$feiertage{'Heilige drei Koenige'}="06.01.";
		$feiertage{'Mariae Himmelfahrt'}="15.08.";
		$feiertage{'Allerheiligen'}="01.11.";
		###
	} ## Ende Bayern

	## Region Hessen ###
	if ($region eq 'Hessen'){
		$wert=$owert+60*$eintag;
		$datum=&getdays($wert);
		$feiertage{'Fronleichnam'}=$datum;
	} ## Ende Hessen

	## Region Nordrhein-Westfalen ###
	if ($region eq 'Nordrhein-Westfalen'){
		$wert=$owert+60*$eintag;
		$datum=&getdays($wert);
		$feiertage{'Fronleichnam'}=$datum;
		###
		$feiertage{'Allerheiligen'}="01.11.";
	} ## Ende Nordrhein-Westfalen

}


return %feiertage;
}


sub getdays{
	my $wert=shift;
	(my $sec,my $min,my$ hour,my $mday,my $mon,my $yr,my $wday,my $yday,my $isdst) = localtime($wert);
	$mon++;
	$yr+=1900;
	my $datum=$mday.'.'.$mon.'.';
	return $datum;
}

sub maketime{
	my $mday=shift;
	my $mon=shift;
	my $jahr=shift;
    return timelocal(0,0,0,$mday,$mon-1,$jahr-1900);
}


1;
